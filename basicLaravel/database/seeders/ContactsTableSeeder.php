<?php
/**
 * 시딩을 수행하기 위한 시더 클래스 생성
 * php artisan make:seeder ContactsTableSeeder
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 시더에서 데이터베이스에 레코드 추가
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'name' => 'Lee Min Kyung',
            'email' => 'love@gmail.com'
        ]);
    }
}
