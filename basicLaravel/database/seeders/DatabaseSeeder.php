<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * 시더 클래스를 생성하면 run() 메서드에서 새롭게 생성한 시더를 호출하도록 call 을 해줘야함.
     * @return void
     */
    public function run()
    {
        $this->call(ContactsTableSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
