<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     * 팩토리가 사용될 모델 클래스
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     * 팩토리를 사용하여 정의될 모델 클래스의 속성값
     *
     * @return array
     */
    public function definition()
    {
        // 모델 팩토리로 생성하는데 글과 댓글 같은 연관 관계에 있는 모델이 함께 생성되어야 할떄
        return [
            'name' => 'lmk',
            'email' => 'lmk@gmail.com',
            'company_id' => function () {
                return App\Models\Company::factory()->create()->id;
            },
            'company_size' => function ($contact) {
                // 바로 위에서 생성한 'company_id' 속성 값을 사용
                return App\Models\Company::find($contact['company_id'])->size;
            }
        ];
    }
}
