<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
    </head>

    <body class="antialiased">
        <!-- old() 헬퍼 함수 사용 시 withInput() 메서드를 사용해 세션에 저장된 입력 값을 얻을 수 있다. -->
        <input name="username" value="<?=old('username', 'Default username instructions here')?>">
    </body>
</html>
