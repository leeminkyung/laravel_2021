<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// 단일 액션 컨트롤러
class UpdateUserAvatar extends Controller
{
    /**
     * 클래스 인스턴스에 호출을 위임해 함수처럼 처리하는 매직 메서드 (라우트 연결)
     */
    public function __invoke()
    {
        // 사용자의 아바타 이미지를 변경..?
    }
}
